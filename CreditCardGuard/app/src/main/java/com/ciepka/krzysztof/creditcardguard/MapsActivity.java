package com.ciepka.krzysztof.creditcardguard;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap googleMap;
    MarkerOptions markerOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ConnectionInProgressActivity.mNotificationSent = false;
    }


    @Override
    public void onMapReady(GoogleMap map) {
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        //map.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        setUpMapIfNeeded();
        GetCurrentLocation();

    }
    private void setUpMapIfNeeded() {
        if (googleMap == null) {

            Log.e("", "Into null map");
            googleMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();

          //  googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
            //        MapsActivity.this));

            if (googleMap != null) {
                Log.e("", "Into full map");
                googleMap.setMapType(googleMap.MAP_TYPE_HYBRID);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
            }
        }
    }

    private void GetCurrentLocation() {

        double[] d = getlocation();


        googleMap
                .addMarker(new MarkerOptions()
                        .position(new LatLng(d[0], d[1]))
                        .title("Telefon"));
                        //.icon(BitmapDescriptorFactory
                               // .fromResource(R.drawable.dot_blue)));

        googleMap.addCircle(new CircleOptions()
                .center(new LatLng(d[0], d[1]))
                .radius(100) // should be RSSI based distance in meters
                .strokeColor(Color.RED)
                .fillColor(Color.TRANSPARENT));

        googleMap
                .animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(d[0], d[1]), 17.0f));
    }
    public double[] getlocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);

        Location l = null;
        for (int i = 0; i < providers.size(); i++) {
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null)
                break;
        }
        double[] gps = new double[2];

        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
        }
        return gps;
    }
}