package com.ciepka.krzysztof.creditcardguard;

public class Item {

    private String title;
    private String description;

    public Item(String title, String description) {
        super();
        this.title = title;
        this.description = description;
    }
    // getters and setters...
    public String getTitle(){
        return this.title;
    }
    public String getDescription(){
        return this.description;
    }
}