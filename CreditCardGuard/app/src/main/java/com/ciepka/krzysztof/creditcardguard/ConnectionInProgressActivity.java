package com.ciepka.krzysztof.creditcardguard;

import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ConnectionInProgressActivity extends Activity {

    public static long RSSI_READ_PERIOD = 1000;
    public static long RSSI_ALERT = -70;
    private final static String TAG = "ConnectionInProgressActivity";
    private BluetoothLeService mBluetoothLeService;
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private String mDeviceName;
    public static boolean mNotificationSent;
    public String mRssi;
    public String str="test";
    private ArrayList<Integer> seriesOfNumbers = new ArrayList<Integer>();
    private String mDeviceAddress;
    private boolean mConnected = false;
    private BluetoothGattCharacteristic characteristicTX;
    private BluetoothGattCharacteristic characteristicRX;
    public final static UUID HM_RX_TX = UUID.fromString(SampleGattAttributes.HM_RX_TX);
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    // define sound URI, the sound to be played when there's a notification
    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    private void sendNotification(){
        Intent resultIntent = new Intent(this, MapsActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("Strażnik Bagażu")
                        .setContentText("Zgubiono bagaż!")
                        .setContentIntent(resultPendingIntent)
                        .setSound(soundUri)
                        //.setOngoing(true)   -- cannot be disabled by the user (clean all)
                        .setLights(0xFFff0000, 100, 100);
        int mId = 001;
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (!mNotificationSent) {
            mNotifyMgr.notify(mId, mBuilder.build());
            mNotificationSent = true;
        }
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            final Handler handler = new Handler();
            final Handler handler2 = new Handler();
            Button mHideButton = (Button) findViewById(R.id.connection_toggle_button);
            TextView mConnectionStateTextView = (TextView) findViewById(R.id.connection_state);
            TextView mRSSITextView = (TextView) findViewById(R.id.rssi_textview);
            TextView mCharacteristicTextView = (TextView) findViewById(R.id.characteristic_read);

            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                mHideButton.setText(R.string.disconnect);
                mConnectionStateTextView.setText(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                mHideButton.setText(R.string.connect);
                mConnectionStateTextView.setText(R.string.disconnected);
                mRSSITextView.setText(R.string.no_rssi);
                mCharacteristicTextView.setText("");
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
                // Characteristic write (currently not used)
                /*final byte[] tx = str.getBytes();
                characteristicTX.setValue(tx);
                mBluetoothLeService.mBluetoothGatt.writeCharacteristic(characteristicTX); */

                // enable indications (automatic data receiving from connected BLE device)
                mBluetoothLeService.setCharacteristicNotification(characteristicRX, true);


            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(mBluetoothLeService.EXTRA_DATA));
                Log.i(TAG, "Characteristic read: " + intent.getStringExtra(mBluetoothLeService.EXTRA_DATA));
            }
            else if (BluetoothLeService.ACTION_RSSI_READ.equals(action)) {
                invalidateOptionsMenu();
                mRssi = mBluetoothLeService.mRssi;
                if (seriesOfNumbers.size() < 20) {
                    seriesOfNumbers.add(Integer.parseInt(mRssi));
                } else {
                    mRSSITextView.setText(Integer.toString((int)average(seriesOfNumbers)) + " dBm  " + "(ok. " + Double.toString(round(getDistance(Integer.parseInt(Integer.toString((int)average(seriesOfNumbers))), -78), 1)) + " m)");
                    seriesOfNumbers.remove(0);
                }
                //mRSSITextView.setText(mRssi + " dBm  " + "(ok. " + Double.toString(round(getDistance(Integer.parseInt(mRssi), -65),1))+ " m)");
                if (Integer.parseInt(mRssi) < RSSI_ALERT){
                    sendNotification();
                }
                Log.i(TAG, "RSSI from BluetoothLeService: " + mRssi);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBluetoothLeService.mBluetoothGatt.readRemoteRssi();
                    }
                }, 100);
            }
        }
    };

    private void displayData(String data) {

        if (data != null) {
            TextView mCharacteristicTextView = (TextView) findViewById(R.id.characteristic_read);
            mCharacteristicTextView.setText(mCharacteristicTextView.getText()+ data);
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static double average(List<Integer> list) {
        // 'average' is undefined if there are no elements in the list.
        if (list == null || list.isEmpty())
            return 0.0;
        // Calculate the summation of the elements in the list
        long sum = 0;
        int n = list.size();
        // Iterating manually is faster than using an enhanced for loop.
        for (int i = 0; i < n; i++)
            sum += list.get(i);
        // We don't want to perform an integer division, so the cast is mandatory.
        return ((double) sum) / n;
    }
    double getDistance(int rssi, int txPower) {
    /*
     * RSSI = TxPower - 10 * n * lg(d)
     * n = 2 (in free space)
     *
     * d = 10 ^ ((TxPower - RSSI) / (10 * n))
     */

        return Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        ActionBar bar = this.getActionBar();
        view.setBackgroundColor(color);
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#595959")));
    }

    public static void buttonEffect(View button){
        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.getBackground().setColorFilter(0xffD3D3D3, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_in_progress);
        setActivityBackgroundColor(0xff000000);
        Button hide_app_button = (Button) findViewById(R.id.hide_app_button);
        Button connection_toggle_button = (Button) findViewById(R.id.connection_toggle_button);

        buttonEffect(hide_app_button);
        buttonEffect(connection_toggle_button);

        final Intent intent = getIntent();

        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);



        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE | BIND_ADJUST_WITH_ACTIVITY);
        if (mBluetoothLeService != null) {
            mBluetoothLeService.connect(mDeviceAddress);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    public void onBackPressed(){
        if(mConnected) {
            moveTaskToBack(true);
        } else{
            Intent intent = new Intent(this, SearchResultsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        RSSI_ALERT = Long.parseLong(settings.getString("RSSI_ALERT", "").toString());
        RSSI_READ_PERIOD = Long.parseLong(settings.getString("RSSI_READ_PERIOD", "").toString());
        getActionBar().setDisplayHomeAsUpEnabled(false);
        if (mBluetoothLeService != null) {
          if (mConnected){
              // restart RSSI readings on resume if GATT connected
              //mBluetoothLeService.mBluetoothGatt.readRemoteRssi();
              mNotificationSent = false;
          }

        }
    }
    public void hide_app_button(View view) {
        moveTaskToBack(true);
    }

    public void connection_toggle_button(View view) {
        if (mBluetoothLeService != null) {
            if (mConnected) {
                mBluetoothLeService.disconnect();
            } else {
                mBluetoothLeService.connect(mDeviceAddress);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_connection_in_progress, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_RSSI_READ);
        return intentFilter;
    }


    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.hide);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();


        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));

            // If the service exists for HM 10 Serial, say so.
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            // get characteristic when UUID matches RX/TX UUID
            characteristicTX = gattService.getCharacteristic(BluetoothLeService.UUID_HM_RX_TX);
            characteristicRX = gattService.getCharacteristic(BluetoothLeService.UUID_HM_RX_TX);
        }

    }

}
