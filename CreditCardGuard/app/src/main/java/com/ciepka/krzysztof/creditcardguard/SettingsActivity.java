package com.ciepka.krzysztof.creditcardguard;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingsActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setActivityBackgroundColor(0xff000000);

        SeekBar mRSSIValueSeekbar = (SeekBar) findViewById(R.id.rssi_value_seek_bar);
        SeekBar mRSSIInntervalSeekbar = (SeekBar) findViewById(R.id.rssi_interval_seek_bar);


        mRSSIValueSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                TextView mRSSIValueTextView = (TextView) findViewById(R.id.RSSIValueTextView);
                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = settings.edit();
                mRSSIValueTextView.setText(Integer.toString(progress - 110) + " dB");
                ConnectionInProgressActivity.RSSI_ALERT = progress -110;
                editor.putString("RSSI_ALERT",Integer.toString(progress - 110));
                editor.commit();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        mRSSIInntervalSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                TextView mRSSIIntervalTextView = (TextView) findViewById(R.id.RSSIIntervalTextView);
                Float f_progress = (float)progress;
                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = settings.edit();
                ConnectionInProgressActivity.RSSI_READ_PERIOD = (progress+1) * 500;
                editor.putString("RSSI_READ_PERIOD", Integer.toString((progress + 1) * 500));
                editor.commit();

                if (progress >  0) {
                    mRSSIIntervalTextView.setText(Float.toString((f_progress+1) * 500/1000) + " s");
                } else{
                    mRSSIIntervalTextView.setText(Integer.toString((progress+1) * 500) + " ms");
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        ActionBar bar = this.getActionBar();
        view.setBackgroundColor(color);
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#595959")));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int RSSIValue;
        int RSSIInterval;
        SeekBar mRSSIValueSeekbar = (SeekBar) findViewById(R.id.rssi_value_seek_bar);
        SeekBar mRSSIInntervalSeekbar = (SeekBar) findViewById(R.id.rssi_interval_seek_bar);

        RSSIValue = Integer.parseInt(settings.getString("RSSI_ALERT", "").toString());
        RSSIInterval = Integer.parseInt(settings.getString("RSSI_READ_PERIOD", "").toString());

        mRSSIValueSeekbar.setProgress(RSSIValue+110);
        mRSSIInntervalSeekbar.setProgress(RSSIInterval/500-1);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
